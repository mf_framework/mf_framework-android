package com.mxnu.mxnuframework.mf_files.Util;

import android.content.Context;
import android.util.Log;

import java.lang.reflect.Field;

/**
 * Created by manuel on 3/02/18.
 */

public class Util {
    private static String TAG = "UTIL";
    public static int LEFT = 0;
    public static int TOP = 1;
    public static int RIGHT = 2;
    public static int BOTTOM = 3;
    public static int[] getMarginParams(String margins){
        //LEFT-TOP-RIGHT-BOTTOM
        String[] margin = margins.split(" ");
        int[] marginsParams = new int[4];
        if(margin.length == 3) {
            marginsParams[LEFT] = marginsParams[RIGHT] = Integer.parseInt(margin[1]);
            marginsParams[TOP] = Integer.parseInt(margin[0]);
            marginsParams[BOTTOM] = Integer.parseInt(margin[2]);
        }
        else if(margin.length == 2){
            marginsParams[TOP] = marginsParams[BOTTOM] = Integer.parseInt(margin[0]);
            marginsParams[LEFT] = marginsParams[RIGHT] = Integer.parseInt(margin[1]);
        }
        else if(margin.length == 1){
            marginsParams[LEFT] = marginsParams[RIGHT] = Integer.parseInt(margin[0]);
            marginsParams[TOP] = marginsParams[BOTTOM] = Integer.parseInt(margin[0]);
        }
        else if(margin.length == 4){
            marginsParams[LEFT] = Integer.parseInt(margin[3]);
            marginsParams[TOP] = Integer.parseInt(margin[0]);
            marginsParams[RIGHT] = Integer.parseInt(margin[1]);
            marginsParams[BOTTOM] = Integer.parseInt(margin[2]);
        }
        else{
            marginsParams[LEFT] = marginsParams[RIGHT] = 0;
            marginsParams[TOP] = marginsParams[BOTTOM] = 0;
        }
        //Log.e(TAG, "" + marginsParams[0] + " " + marginsParams[1] + " " + marginsParams[2] + " " + marginsParams[3]);
        return marginsParams;
    }
    public static int getResId(Context context, String resName) {
        return context.getResources().getIdentifier(resName, "drawable", context.getPackageName());
    }
}
