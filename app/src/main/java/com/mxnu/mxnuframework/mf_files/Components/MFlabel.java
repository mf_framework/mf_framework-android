package com.mxnu.mxnuframework.mf_files.Components;

import android.content.Context;
import android.graphics.Color;
import android.widget.Button;
import android.widget.TextView;

import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.Ajax;
import com.mxnu.mxnuframework.mf_files.Util.MF;
import com.mxnu.mxnuframework.mf_files.Util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by manuel on 4/02/18.
 */

public class MFlabel extends ComponentBase {
    @Override
    protected void setup() {
        super.setup();
        String value = null;
        String textColor = null;
        try {
            if(jsonView.has("value")){
                value = jsonView.getString("value");
            }
            if(jsonView.has("textColor")){
                textColor = jsonView.getString("textColor");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(value != null){
            String val = MF.parse(value, this);
            ((TextView)view).setText(val);
        }
        if(textColor != null){
            ((TextView)view).setTextColor(Color.parseColor(textColor));
        }
    }

    public MFlabel(Context context, JSONObject jsonView, OnCallbackClick onCallbackClick, CustomAdapter customAdapter) {
        super(context, jsonView, onCallbackClick, customAdapter);
        this.view = new TextView(context);
        setup();
    }
}
