package com.mxnu.mxnuframework.mf_files.Layouts;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mxnu.mxnuframework.mf_files.Components.ComponentBase;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.ObjectResolver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by manuel on 31/01/18.
 */

public class LayoutBasic extends ComponentBase {
    private static String TAG = "LAYOUTBASIC";
    private Map<String, Map<String, ComponentBase>> _childs;

    public LayoutBasic(Context context, JSONObject jsonLayout, OnCallbackClick onCallbackClick, CustomAdapter customAdapter) {
        super(context, jsonLayout, onCallbackClick, customAdapter);
        this.jsonView = jsonLayout;
        this._childs = new HashMap<>();
    }

    @Override
    protected void setup() {
        super.setup();
        addChilds();
    }

    private void addChilds() {
        try {
            JSONArray childs = jsonView.getJSONArray("childs");
            for (int i = 0; i < childs.length(); i++) {
                JSONObject jsonComponent = childs.getJSONObject(i);
                String mf_type = jsonComponent.getString("mf-type");
                ComponentBase component = (ComponentBase) ObjectResolver.getViewInstance(mf_type, context, jsonComponent, onCallackClick, customAdapter);
                component.setParent(this);
                View componentView = component.createView();
                if (componentView != null) {
                    addView(componentView);
                    if(_childs.containsKey(mf_type)){
                        _childs.get(mf_type).put(component.getName(),component);
                    }
                    else {
                        _childs.put(mf_type, new HashMap<String, ComponentBase>());
                        _childs.get(mf_type).put(component.getName(),component);
                    }
                }
                else{
                    Log.e(TAG, "component nulo");
                }
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public ComponentBase down(String name){
        Iterator<Map.Entry<String, Map<String, ComponentBase>>> typesIterator = _childs.entrySet().iterator();
        while (typesIterator.hasNext()){
            Map.Entry<String, Map<String, ComponentBase>> currentType = typesIterator.next();
            Iterator<Map.Entry<String, ComponentBase>> namesIterator = currentType.getValue().entrySet().iterator();
            while (namesIterator.hasNext()){
                Map.Entry<String, ComponentBase> componentMap = namesIterator.next();
                if(componentMap.getKey().equals(name)){
                    return componentMap.getValue();
                }
                else if (componentMap.getValue() instanceof LayoutBasic){
                    ComponentBase res = ((LayoutBasic)componentMap.getValue()).down(name);
                    if(res != null){
                        return res;
                    }
                }
            }
        }
        return null;
    }

    public ComponentBase down(String type, String name){
        if(_childs.containsKey(type)){
            Map<String, ComponentBase> currentType = _childs.get(type);
            Iterator<Map.Entry<String, ComponentBase>> namesIterator = currentType.entrySet().iterator();
            while (namesIterator.hasNext()){
                Map.Entry<String, ComponentBase> componentMap = namesIterator.next();
                if(componentMap.getKey().equals(name)){
                    return componentMap.getValue();
                }
                else if (componentMap.getValue() instanceof LayoutBasic){
                    ComponentBase res = ((LayoutBasic)componentMap.getValue()).down(name);
                    if(res != null){
                        return res;
                    }
                }
            }
        }
        return null;
    }

    protected void addView(View view){
        ((ViewGroup)this.view).addView(view);
    }
}
