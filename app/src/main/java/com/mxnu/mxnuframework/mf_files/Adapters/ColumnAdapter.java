package com.mxnu.mxnuframework.mf_files.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mxnu.mxnuframework.R;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.CustomAdapterEnum;
import com.mxnu.mxnuframework.mf_files.Models.JsonDataContainer;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manuel on 27/03/18.
 */

public class ColumnAdapter extends RecyclerView.Adapter<ColumnAdapter.ViewHolder> {

    private static final String TAG = "COLUMNADAPTER";
    private JsonDataContainer dataContainer;
    private Context context;
    private RecyclerView recyclerView;
    public static final String COLUMN_NAME = "COLUMN_DATA";
    public static final String WIDTH = "WIDTH";
    private CustomAdapter customAdapter;
    private int rowPosition;
    private String name;

    public ColumnAdapter(Context context, RecyclerView recyclerView, CustomAdapter customAdapter, String name) {
        this.context = context;
        this.recyclerView = recyclerView;
        this.dataContainer = new JsonDataContainer();
        this.customAdapter = customAdapter;
        this.name = name;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mf_table_column_item, parent,false);
        return new ViewHolder(view);
    }

    private int getHeight(View view, int width){
        view.measure(View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        return view.getMeasuredHeight();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        JSONObject current = dataContainer.getData().get(position);
        try {
            holder.txt_table_column.setText(current.getString(COLUMN_NAME));
            holder.setWidth(current.getInt(WIDTH));
            if(current.has("MAX")){
                ViewGroup.LayoutParams layoutParams = recyclerView.getLayoutParams();
                layoutParams.height = getHeight(holder.view, current.getInt(WIDTH));
                recyclerView.setLayoutParams(layoutParams);
            }
            if(customAdapter != null){
                customAdapter.onBind(name, holder, position, CustomAdapterEnum.TABLE | CustomAdapterEnum.VERTICAL);
                customAdapter.onBind(name, holder, rowPosition, CustomAdapterEnum.TABLE | CustomAdapterEnum.HORIZONTAL);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataContainer.getCount();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView txt_table_column;
        public CardView table_column_item;
        View view;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            txt_table_column = view.findViewById(R.id.txt_table_column);
            table_column_item = view.findViewById(R.id.table_column_item);
        }
        public void setWidth(int width){
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.width = width;
            view.setLayoutParams(params);
        }
    }
    public void updateData(JsonDataContainer jsonDataContainer, int rowPosition){
        this.dataContainer = jsonDataContainer;
        this.rowPosition = rowPosition;
        notifyDataSetChanged();
    }
}
