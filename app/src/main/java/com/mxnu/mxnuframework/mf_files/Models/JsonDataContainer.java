package com.mxnu.mxnuframework.mf_files.Models;

import com.mxnu.mxnuframework.mf_files.Adapters.ListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by manuel on 30/01/18.
 */

public class JsonDataContainer {
    protected String title;
    protected Map<String, JSONObject> fields;
    protected List<JSONObject> data;
    protected String _fieldCode = null;
    protected String _fieldName = null;

    public JsonDataContainer() {
        data = new ArrayList<>();
        fields = new LinkedHashMap<>();
    }

    public Map<String, JSONObject> getFields() {
        return fields;
    }

    public void setFields(List<JSONObject> fields) {
        this.fields.clear();
        for (JSONObject i: fields){
            try {
                this.fields.put(i.getString("dataIndex"), i);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public List<JSONObject> getData() {
        return data;
    }

    public void setData(List<JSONObject> data) {
        this.data = data;
    }

    public String getFieldCode() {
        return _fieldCode;
    }

    public void setFieldCode(String _fieldCode) {
        this._fieldCode = _fieldCode;
    }

    public String getFieldName() {
        return _fieldName;
    }

    public void setFieldName(String _fieldName) {
        this._fieldName = _fieldName;
    }

    public int getCount(){
        return data.size();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
