package com.mxnu.mxnuframework.mf_files.Components;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItem;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;

import com.mxnu.mxnuframework.R;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.RestClient;
import com.mxnu.mxnuframework.mf_files.Util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by manu2 on 08/02/2018.
 */

public class MFmodern_bar extends ComponentBase implements BottomNavigationView.OnNavigationItemSelectedListener{
    private static String TAG = "MODERN_BAR";
    private BottomNavigationView bottomNavigationView;
    private Map<Integer, FrameModernBar> fragmentos = new LinkedHashMap<>();
    public MFmodern_bar(Context context, final JSONObject jsonView, OnCallbackClick onCallbackClick, CustomAdapter customAdapter) {
        super(context, jsonView, onCallbackClick, customAdapter);
        this.view = LayoutInflater.from(context).inflate(R.layout.mf_modern_bar, null, false);
        bottomNavigationView = this.view.findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        FrameModernBar first;
        try {
            JSONArray childs = jsonView.getJSONArray("childs");
            for(int i = 0; i < childs.length(); i++){
                JSONObject jsonActionView = childs.getJSONObject(i);
                String title = "-";
                String icon = null;
                if(jsonActionView.has("title")){
                    title = jsonActionView.getString("title");
                }
                if(jsonActionView.has("icon")){
                    icon = jsonActionView.getString("icon");
                }
                MenuItem item = bottomNavigationView.getMenu().add(0, Menu.FIRST + i, Menu.FIRST, title);
                if(icon != null){
                    item.setIcon(Util.getResId(context, icon));
                }
                FrameModernBar fragment =  new FrameModernBar();
                Bundle args = new Bundle();
                args.putString("jsonView", jsonActionView.toString());
                fragment.setArguments(args);
                fragmentos.put(item.getItemId(), fragment);
                fragment.setOnCallbackClick(onCallbackClick);
                fragment.setCustomAdapter(customAdapter);
            }
        } catch (JSONException e){
            e.printStackTrace();
        }
        bottomNavigationView.setSelectedItemId(fragmentos.entrySet().iterator().next().getKey());
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.actionView, fragmentos.get(item.getItemId())).commit();
        return true;
    }
}
