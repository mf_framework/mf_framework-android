package com.mxnu.mxnuframework.mf_files.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mxnu.mxnuframework.R;
import com.mxnu.mxnuframework.mf_files.Models.JsonDataContainer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

/**
 * Created by manuel on 23/03/18.
 */

public class ComboAdapter extends ArrayAdapter<String>{
    private static final String TAG = "COMBOADAPTER";
    private JsonDataContainer dataContainer;

    public ComboAdapter(Context context, int resource) {
        super(context, resource);
        dataContainer = new JsonDataContainer();
    }

    @Override
    public int getCount() {
        return dataContainer.getCount();
    }

    @Override
    public String getItem(int position) {
        try {
            return dataContainer.getData().get(position).getString(dataContainer.getFieldCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.mf_combo_item, parent, false);

        Iterator<String> iterador = dataContainer.getFields().keySet().iterator();
        String text = "";
        while (iterador.hasNext())
        {
            String iter = iterador.next();
            try {
                String key = dataContainer.getFields().get(iter).getString("dataIndex");
                text += " " + dataContainer.getData().get(position).getString(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ((TextView)view.findViewById(R.id.txt_spinner_item)).setText(text);
        return view;
        //return super.getDropDownView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.mf_combo_item, parent, false);

        Iterator<String> iterador = dataContainer.getFields().keySet().iterator();
        String text = "";
        while (iterador.hasNext())
        {
            String iter = iterador.next();
            try {
                String key = dataContainer.getFields().get(iter).getString("dataIndex");
                text += " " + dataContainer.getData().get(position).getString(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ((TextView)view.findViewById(R.id.txt_spinner_item)).setText(text);
        return view;
        //return super.getView(position, convertView, parent);
    }

    public void updateList(List<JSONObject> list){
        dataContainer.setData(list);
        notifyDataSetChanged();
    }

    public void setDataContainer(JsonDataContainer dataContainer){
        this.dataContainer = dataContainer;
    }
}
