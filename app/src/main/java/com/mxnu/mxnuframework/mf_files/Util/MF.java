package com.mxnu.mxnuframework.mf_files.Util;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.util.Log;

import com.mxnu.mxnuframework.mf_files.Components.ComponentBase;
import com.mxnu.mxnuframework.mf_files.Components.RequestComponent;
import com.mxnu.mxnuframework.mf_files.Layouts.LayoutBasic;
import com.mxnu.mxnuframework.mf_files.Layouts.MFLinearLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;

/**
 * Created by manuel on 22/03/18.
 */

public class MF {
    private static String TAG = "MF";
    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
    public static String parse(String params, ComponentBase componentBase){
        String returnValue = params;
        try{
            String[] values = params.split("\\$");
            String response = "";
            Log.e(TAG, "value length_" + values.length);
            if(values.length == 3){
                ComponentBase currentInstance = ((LayoutBasic)componentBase.getRoot()).down(values[1]);
                response = currentInstance.getValue();
            }
            else if(values.length == 1){
                response = values[0];
            }
            else if(values.length >= 3){
                if(params.contains("$MF$Session$")){
                    Log.e(TAG, "session");
                    JSONObject session = Sessions.getInstance().read(values[3]);
                    Log.e(TAG, session.toString());
                    response = session.getString(values[4]);
                    Log.e(TAG, response);
                }
            }
            returnValue = response;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*if(values.length == 0){
            return params;
        }
        switch (values[1]){
            case "response":
                break;
        }*/
        return returnValue;
    }
    public static void doAction(ComponentBase componentBase, String action, JSONObject actions){
        JSONObject params;
        switch (action) {
            case "callback":
                try {
                    int id = actions.getInt("id");
                    if(componentBase instanceof RequestComponent){
                        JSONArray data = ((RequestComponent)componentBase).getData();
                        componentBase.onCallackClick.onCallbackClick(id, data);
                    }
                    else{
                        componentBase.onCallackClick.onCallbackClick(id, componentBase.getValue());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case "clear-session":
                Sessions.getInstance().clear();
                break;
            case "save-session":
                String id = null;
                String data = null;
                try{
                    params = actions.getJSONObject("params");
                    JSONArray sessions = params.getJSONArray("params");
                    Log.e(TAG, sessions.toString());
                    for (int i = 0; i < sessions.length(); i++){
                        if(sessions.getJSONObject(i).has("id")){
                            id = sessions.getJSONObject(i).getString("id");
                        }
                        if(sessions.getJSONObject(i).has("data")){
                            data = sessions.getJSONObject(i).getString("data");
                        }
                        if(id != null && data != null && data.contains("$MF$response$getValue")){
                            //EDITAR_EDITAR
                            Sessions.getInstance().write(id, actions.getJSONObject("response"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case "alert":
                try {
                    Log.e(TAG, "alert");
                    params = actions.getJSONObject("params");
                    String text = params.getString("text");
                    Snackbar.make(componentBase.getRoot().createView(), text, Snackbar.LENGTH_LONG).show();
                    //((LayoutBasic)getRoot()).down("btnAceptar").view.setBackgroundColor(Color.RED);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case "launch":
                try {
                    params = actions.getJSONObject("params");
                    if (params.has("activity")) {
                        Intent intent = new Intent(
                                //componentBase.getContext().getPackageName() +
                                "com.mxnu.mxnuframework" +
                                "." +
                                params.getString("activity")
                        );
                        componentBase.getContext().startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case "selector":
                try {
                    params = actions.getJSONObject("params");
                    ComponentBase cb = ((LayoutBasic)componentBase.getRoot()).down(params.getString("name"));
                    ((RequestComponent)cb).setParams(params.getJSONObject("params"));
                    ((RequestComponent)cb).load();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
