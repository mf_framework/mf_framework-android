package com.mxnu.mxnuframework.mf_files.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mxnu.mxnuframework.R;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.CustomAdapterEnum;
import com.mxnu.mxnuframework.mf_files.Models.JsonDataContainer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by manuel on 26/03/18.
 */

public class TableAdapter extends RecyclerView.Adapter<TableAdapter.ViewHolder> {
    private List<JsonDataContainer> tableData;
    private Context context;
    private static String TAG = "TABLEADAPTER";
    private CustomAdapter customAdapter;
    private String name;

    public TableAdapter(Context context, CustomAdapter customAdapter, String name){
        this.context = context;
        this.tableData = new ArrayList<>();
        this.customAdapter = customAdapter;
        this.name = name;
    }

    @Override
    public TableAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.mf_table_item,
                parent,
                false);
        ViewHolder holder = new TableAdapter.ViewHolder(view, customAdapter);
        //holder.recyclerView_table_column.setHasFixedSize(false);
        return holder;
    }

    @Override
    public void onBindViewHolder(TableAdapter.ViewHolder holder, int position) {
        JsonDataContainer current = tableData.get(position);
        holder.adapter.updateData(current, position);
    }

    @Override
    public int getItemCount() {
        return tableData.size();
    }

    public void updateData(List<JsonDataContainer> tableData){
        this.tableData = tableData;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ColumnAdapter adapter;
        public RecyclerView recyclerView_table_column;
        public View main_table_item;

        public ViewHolder(View itemView, CustomAdapter customAdapter) {
            super(itemView);
            this.main_table_item = itemView;
            bind();
            adapter = new ColumnAdapter(context, recyclerView_table_column, customAdapter, name);
            setupAdapter();
        }

        private void setupAdapter() {
            recyclerView_table_column.setLayoutManager(new LinearLayoutManager(context,
                    LinearLayoutManager.HORIZONTAL, false));
            recyclerView_table_column.setAdapter(adapter);
        }

        private void bind() {
            recyclerView_table_column = main_table_item.findViewById(R.id.recyclerView_table_column);
        }

    }

}
