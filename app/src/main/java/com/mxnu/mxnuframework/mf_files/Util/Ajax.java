package com.mxnu.mxnuframework.mf_files.Util;

import com.mxnu.mxnuframework.mf_files.Components.ComponentBase;
import com.mxnu.mxnuframework.mf_files.Layouts.LayoutBasic;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by manu2 on 12/02/2018.
 */

public class Ajax {
    private static String TAG = "AJAX";
    public static String getParams(JSONObject jsonParams, ComponentBase componentBase){
        String params = "";
        boolean first = true;
        try{
            Iterator<String> iterator = jsonParams.keys();
            while(iterator.hasNext()){
                String key = iterator.next();
                String param = jsonParams.getString(key);
                String []values = param.split("\\$");
                String response = "";
                if(values.length == 3){
                    ComponentBase currentInstance = ((LayoutBasic)componentBase.getRoot()).down(values[1]);
                    response = currentInstance.getValue();
                }
                else if(values.length == 1){
                    response = values[0];
                }
                else if(values.length >= 3){
                    if(param.contains("$MF$Session$")) {
                        JSONObject session = Sessions.getInstance().read(values[3]);
                        response = session.getString(values[4]);
                    }
                }
                if(first){
                    first = false;
                }
                else{
                    params += "&";
                }
                params += key + "=" + response;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }
}
