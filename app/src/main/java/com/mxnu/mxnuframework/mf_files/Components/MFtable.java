package com.mxnu.mxnuframework.mf_files.Components;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mxnu.mxnuframework.R;
import com.mxnu.mxnuframework.mf_files.Adapters.ColumnAdapter;
import com.mxnu.mxnuframework.mf_files.Adapters.TableAdapter;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.Models.JsonDataContainer;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.Ajax;
import com.mxnu.mxnuframework.mf_files.Util.IRestResult;
import com.mxnu.mxnuframework.mf_files.Util.MF;
import com.mxnu.mxnuframework.mf_files.Util.RestClient;
import com.mxnu.mxnuframework.mf_files.Util.Sessions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manuel on 26/03/18.
 */

public class MFtable extends RequestComponent implements IRestResult{
    private TableAdapter adapter;
    private JSONArray columns;
    private ProgressBar tableProgressBar;
    private static String TAG = "MFTABLE";
    private static final int DATA_RESPONSE = 0;

    public MFtable(Context context, JSONObject jsonView, OnCallbackClick onCallbackClick, CustomAdapter customAdapter) {
        super(context, jsonView, onCallbackClick, customAdapter);
        this.view = LayoutInflater.from(context).inflate(R.layout.mf_table, null, false);
        tableProgressBar = this.view.findViewById(R.id.tableProgressBar);
        setup();
    }

    @Override
    protected void setup()
    {
        super.setup();
        setupAdapter();
        JSONObject data_container = null;
        try{
            if(jsonView.has("data_container")){
                data_container = jsonView.getJSONObject("data_container");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(data_container != null){
            fillMftable(data_container);
        }
    }

    @Override
    public View createView() {
        if(jsonView.has("title")){
            try {
                ((TextView)view.findViewById(R.id.txtTitle)).setText(jsonView.getString("title"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return view;
        }
        else{
            return super.createView();
        }
    }

    private void fillMftable(JSONObject data_container) {
        List<JsonDataContainer> tableData = new ArrayList<>();
        try{
            columns = data_container.getJSONArray("columns");
            LinearLayout table_headers_container = this.view.findViewById(R.id.table_headers_container);
            int totalWidth = 0;
            for (int i = 0; i < columns.length(); i++){
                JSONObject current = columns.getJSONObject(i);
                current.put(ColumnAdapter.WIDTH, 500);
                TextView headerText = new TextView(context);
                headerText.setText(current.getString("text"));
                headerText.setWidth(500);
                LinearLayout.LayoutParams text_params = new LinearLayout.LayoutParams(500, ViewGroup.LayoutParams.WRAP_CONTENT);
                text_params.setMargins(3, 3, 3, 3);
                text_params.gravity = Gravity.CENTER_HORIZONTAL;
                headerText.setLayoutParams(text_params);
                totalWidth += 500 + 6;
                table_headers_container.addView(headerText);
            }
            ViewGroup.LayoutParams headers_params = table_headers_container.getLayoutParams();
            headers_params.width = totalWidth;

            if(data_container.has("data")){
                JSONArray data = data_container.getJSONArray("data");
                loadLocalData(data);
            }

            if(data_container.has("persistant")){
                JSONObject persistant = data_container.getJSONObject("persistant");
                Sessions sessions = Sessions.getInstance();
                JSONObject dataContainer = sessions.read(persistant.getString("name"));
                if(dataContainer != null){
                    JSONArray data = dataContainer.getJSONArray("data");
                    loadLocalData(data);
                    return;
                }
            }

            if(data_container.has("ajax")){
                JSONObject ajax = data_container.getJSONObject("ajax");
                String url = ajax.getString("url");
                this.url = url;
                JSONObject params = null;
                if(ajax.has("params")){
                    params = data_container.getJSONObject("ajax").getJSONObject("params");
                    this.params = params;
                }
                else{
                    this.params = null;
                }
                if(!ajax.has("autoLoad") || ajax.getBoolean("autoLoad")){
                    load();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //adapter.updateData(tableData);
    }

    private void loadLocalData(JSONArray data){
        List<JsonDataContainer> tableData = new ArrayList<>();
        try {
            for(int i = 0; i < data.length(); i++) {
                JSONObject currentRow = data.getJSONObject(i);
                JsonDataContainer tmpContainer = new JsonDataContainer();
                int maxHeight = 0;
                int maxHeightIndex = 0;
                for(int j = 0; j < columns.length(); j++) {
                    JSONObject currentColumn = columns.getJSONObject(j);

                    JSONObject tmpJSON = new JSONObject();
                    String text = currentRow.getString(currentColumn.getString("dataIndex"));
                    tmpJSON.put(ColumnAdapter.COLUMN_NAME, text);
                    tmpJSON.put(ColumnAdapter.WIDTH, currentColumn.getInt(ColumnAdapter.WIDTH));

                    int currentHeight = text.length() + (text.split("\\n").length - 1) * 2;
                    if(maxHeight < currentHeight){
                        maxHeight = currentHeight;
                        maxHeightIndex = j;
                    }

                    tmpContainer.getData().add(tmpJSON);
                }
                tmpContainer.getData().get(maxHeightIndex).put("MAX", true);
                tableData.add(tmpContainer);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
        adapter.updateData(tableData);
    }

    @Override
    public void setParams(JSONObject params) {
        this.params = params;
    }

    @Override
    public void load() {
        tableProgressBar.setVisibility(View.VISIBLE);
        RestClient client = new RestClient();
        if(params != null){
            client.execute(url, Ajax.getParams(params, this), DATA_RESPONSE, this);
        }
        else{
            client.execute(url, DATA_RESPONSE, this);
        }
    }

    private void setupAdapter() {
        adapter = new TableAdapter(context, customAdapter, name);

        ((RecyclerView)this.view.findViewById(R.id.recyclerViewTable)).setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        ((RecyclerView)this.view.findViewById(R.id.recyclerViewTable)).setAdapter(adapter);
    }

    @Override
    public void onSuccess(int code, String response) {
        switch (code){
            case DATA_RESPONSE:
                tableProgressBar.setVisibility(View.GONE);
                try{
                    JSONObject res = new JSONObject(response);
                    if(!res.getBoolean("success")){
                        return;
                    }

                    LinearLayout table_headers_container = this.view.findViewById(R.id.table_headers_container);
                    int totalWidth = 0;
                    for (int i = 0; i < columns.length(); i++){
                        JSONObject current = columns.getJSONObject(i);
                        current.put(ColumnAdapter.WIDTH, 500);
                        TextView headerText = new TextView(context);
                        headerText.setText(current.getString("text"));
                        headerText.setWidth(500);
                        LinearLayout.LayoutParams text_params = new LinearLayout.LayoutParams(500, ViewGroup.LayoutParams.WRAP_CONTENT);
                        text_params.setMargins(3, 3, 3, 3);
                        text_params.gravity = Gravity.CENTER_HORIZONTAL;
                        headerText.setLayoutParams(text_params);
                        totalWidth += 500 + 6;
                        table_headers_container.addView(headerText);
                    }
                    ViewGroup.LayoutParams headers_params = table_headers_container.getLayoutParams();
                    headers_params.width = totalWidth;

                    JSONArray data = res.getJSONArray("data");
                    requestData = data;
                    JSONObject data_container = jsonView.getJSONObject("data_container");
                    if(data_container.has("persistant")){
                        JSONObject persistant = data_container.getJSONObject("persistant");
                        if(Sessions.getInstance().read(persistant.getString("name")) == null){
                            JSONObject data_to_save = new JSONObject();
                            data_to_save.put("data", data);
                            Sessions.getInstance().write(persistant.getString("name"), data_to_save);
                        }
                    }
                    loadLocalData(data);
                    JSONArray actions = getEvents(LOAD_EVENT);
                    if(actions != null){
                        for(int i = 0; i < actions.length(); i++){
                            MF.doAction(
                                this,
                                actions.getJSONObject(i).getString("action"),
                                actions.getJSONObject(i)
                            );
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private JSONArray getEvents(int loadEvent) {
        switch (loadEvent){
            case LOAD_EVENT:
                try {
                    JSONObject data_container = jsonView.getJSONObject("data_container");
                    JSONObject ajax = data_container.getJSONObject("ajax");
                    JSONObject events = ajax.getJSONObject("events");
                    JSONArray onLoad = events.getJSONArray("onLoad");
                    return onLoad;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
        return null;
    }

    private static final int LOAD_EVENT = 0;

    @Override
    public void onError(int code, String error) {
        tableProgressBar.setVisibility(View.GONE);
        Snackbar.make(this.getRoot().view, error, Snackbar.LENGTH_LONG).show();

    }
}
