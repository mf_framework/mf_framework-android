package com.mxnu.mxnuframework.mf_files.Components;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mxnu.mxnuframework.R;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.MF;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manuel on 5/03/18.
 */

public class MFweb_view extends ComponentBase {
    private WebView webView;
    private WebView webViewProgress;

    @Override
    public View createView() {
        //return new LinearLayout(this.context);
        return super.createView();
    }

    public MFweb_view(Context context, JSONObject jsonView, OnCallbackClick onCallackClick, CustomAdapter customAdapter) {
        super(context, jsonView, onCallackClick, customAdapter);
        //this.view = new WebView(context);
        this.view = LayoutInflater.from(this.context).inflate(R.layout.mf_webview, null, false);
        bind();
        //((WebView)this.view).getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        setup();
    }

    private void bind() {
        webView = this.view.findViewById(R.id.webview);
        //webViewProgress = this.view.findViewById(R.id.webview_progress);
    }

    @Override
    protected void setup() {
        super.setup();
        try {
            if(jsonView.has("url")){
                if(!MF.isOnline(context)){
                    showError("Sin acceso a internet");
                    return;
                }
                String url = jsonView.getString("url");
                webView.setWebViewClient(new WebViewClient(){
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        //webViewProgress.setVisibility(View.GONE);
                    }
                });
                webView.loadUrl(url);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showError(String error){
        //Snackbar.make(this.view, error, Snackbar.LENGTH_SHORT).show();
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
    }
}
