package com.mxnu.mxnuframework.mf_files.Components;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;

import com.mxnu.mxnuframework.R;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.Ajax;
import com.mxnu.mxnuframework.mf_files.Util.IRestResult;
import com.mxnu.mxnuframework.mf_files.Util.MF;
import com.mxnu.mxnuframework.mf_files.Util.RestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manuel on 1/02/18.
 */

public class MFbutton extends ComponentBase implements View.OnClickListener {
    private static String TAG = "MFBUTTON";
    public MFbutton(Context context, JSONObject jsonView, OnCallbackClick onCallbackClick, CustomAdapter customAdapter) {
        super(context, jsonView, onCallbackClick, customAdapter);
        this.view = new Button(context);
        setup();
    }

    @Override
    protected void setup() {
        super.setup();
        String text = null;
        String textColor = null;
        String textSize = null;
        String color = null;
        String style = null;
        String image = null;
        try {
            if(jsonView.has("text")){
                text = jsonView.getString("text");
            }
            if(jsonView.has("textColor")){
                textColor = jsonView.getString("textColor");
            }
            if (jsonView.has("color")){
                color = jsonView.getString("color");
            }
            if(jsonView.has("style")){
                style = jsonView.getString("style");
            }
            if(jsonView.has("image")){
                image = jsonView.getString("image");
            }
            if(jsonView.has("textSize")){
                textSize = jsonView.getString("textSize");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(text != null){
            ((Button)view).setText(text);
        }
        if(textColor != null){
            ((Button)view).setTextColor(Color.parseColor(textColor));
        }
        if(color == null){
            view.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
        }
        if(style != null){
            switch (style){
                case "simple":
                    view.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
                    break;
            }
        }
        if(textSize != null){
            int size = Integer.parseInt(textSize);
            ((Button)view).setTextSize(size);
        }
        if(jsonView.has("actions")){
            this.view.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        try{
            JSONArray actions = jsonView.getJSONArray("actions");
            for (int i = 0; i < actions.length(); i++){
                JSONObject action = actions.getJSONObject(i);
                MF.doAction(this, action.getString("action"), action);
                if(action.getString("action").equals("ajax")){
                    final JSONObject ajaxOpt = action.getJSONObject("params");
                    String params = Ajax.getParams(ajaxOpt.getJSONObject("params"), this);
                    RestClient restClient = new RestClient();
                    restClient.execute(ajaxOpt.getString("url"), params, 0, new IRestResult() {
                        @Override
                        public void onSuccess(int code, String response) {
                            try{
                                JSONObject jsonResponse = new JSONObject(response);
                                if(jsonResponse.getBoolean("success")){
                                    JSONArray success = ajaxOpt.getJSONArray("success");
                                    for(int i = 0; i < success.length(); i++){
                                        success.getJSONObject(i).put("response", jsonResponse.getJSONObject("data"));
                                        MF.doAction(
                                                MFbutton.this,
                                                success.getJSONObject(i).getString("action"),
                                                success.getJSONObject(i)
                                        );
                                    }
                                }
                                else{
                                    onError(code, "");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(int code, String error) {
                            try{
                                //showError(error);
                                JSONArray jsonError = ajaxOpt.getJSONArray("failed");
                                for(int i = 0; i < jsonError.length(); i++){
                                    MF.doAction(
                                            MFbutton.this,
                                            jsonError.getJSONObject(i).getString("action"),
                                            jsonError.getJSONObject(i)
                                    );
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
