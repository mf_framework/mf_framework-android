package com.mxnu.mxnuframework.mf_files.Util;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by manuel on 4/02/18.
 */

public class RestClient {
    private static String TAG = "RESTCLIENT";
    /*private static class SingletonHolder{
        public static final RestClient INSTANCE = new RestClient();
    }
    public static RestClient getInstance(){
        return SingletonHolder.INSTANCE;
    }*/
    private IRestResult iRestResult;
    public void execute(String url, int code, IRestResult iRestResult){
        this.iRestResult = iRestResult;
        new RestGetSet().execute(new String[]{url, String.valueOf(code)});
    }
    public void execute(String url, String params, int code, IRestResult iRestResult){
        this.iRestResult = iRestResult;
        new RestGetSet().execute(new String[]{url, String.valueOf(code), params});
    }

    private class RestGetSet extends AsyncTask<String, JSONObject, JSONObject>{

        @Override
        protected JSONObject doInBackground(String... params) {
            int code = Integer.parseInt(params[1]);
            try {
                URL urlService = new URL(params[0]);
                HttpURLConnection service = (HttpURLConnection) urlService.openConnection();
                service.setRequestMethod("POST");
                if(params.length > 2){
                    String data = params[2];
                    service.setDoOutput(true);
                    OutputStream outputStream = new BufferedOutputStream(service.getOutputStream());
                    outputStream.write(data.getBytes());
                    outputStream.flush();
                    outputStream.close();
                }
                BufferedReader br = null;
                StringBuilder sb = new StringBuilder();
                if(service.getResponseCode() == 200){
                    BufferedReader br2 = new BufferedReader(new InputStreamReader(service.getInputStream()));
                    String line = br2.readLine();
                    if(line != null){
                        sb.append(line);
                    }
                    br2.close();
                    br = br2;
                }
                String stringBuilder = sb.toString();
                if(service != null){
                    service.disconnect();
                }
                if(br == null){
                    return null;
                }
                br.close();
                Map<String, Object> result = new HashMap<>();
                result.put("success", true);
                result.put("code", code);
                result.put("response", stringBuilder);
                JSONObject res = new JSONObject(result);
                //Log.e(TAG, res.toString());
                return res;
            } catch (Exception e) {
                e.printStackTrace();
                Map<String, Object> result = new HashMap<>();
                result.put("success", false);
                result.put("code", code);
                result.put("response", e.getMessage());
                JSONObject res = new JSONObject(result);
                return res;
            }
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            //super.onPostExecute(jsonObject);
            if(jsonObject == null){
                return;
            }
            try{
                Log.e(TAG, jsonObject.toString());
                if(jsonObject.getBoolean("success")){
                    iRestResult.onSuccess(jsonObject.getInt("code"), jsonObject.getString("response"));
                }
                else{
                    iRestResult.onError(jsonObject.getInt("code"), jsonObject.getString("response"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
