package com.mxnu.mxnuframework.mf_files.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.mxnu.mxnuframework.R;
import com.mxnu.mxnuframework.mf_files.Components.MFlist;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.CustomAdapterEnum;
import com.mxnu.mxnuframework.mf_files.Models.JsonDataContainer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by manuel on 30/01/18.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder>{
    private JsonDataContainer dataContainer;
    private Context context;
    private CustomAdapter customAdapter;
    private String name;
    private ViewHolder current;
    private OnItemListClickListener onItemClickListener;
    private static String TAG = "LISTADAPTER";

    public ListAdapter(Context context, CustomAdapter customAdapter, String name) {
        this.context = context;
        dataContainer = new JsonDataContainer();
        this.customAdapter = customAdapter;
        this.name = name;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mf_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        JSONObject json = dataContainer.getData().get(position);
        if(dataContainer.getFieldName() != null){
            try {
                String data = json.getString(dataContainer.getFieldName());
                holder.txt_list_item_title.setText(data);
                holder.setOnClickListener(dataContainer.getData().get(position));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        customAdapter.onBind(name, holder, position, CustomAdapterEnum.LIST);
    }

    @Override
    public int getItemCount() {
        return dataContainer.getCount();
    }

    public void setOnItemClickListener(OnItemListClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView txt_list_item_title;
        private LinearLayout list_item_details;
        private View main_list_item;
        public ViewHolder(View itemView) {
            super(itemView);
            this.main_list_item = itemView;
            bind();
        }

        private void bind() {
            txt_list_item_title = main_list_item.findViewById(R.id.txt_list_item_title);
            list_item_details = main_list_item.findViewById(R.id.list_item_details);
        }

        public void setOnClickListener(final JSONObject item){
            main_list_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(list_item_details.getVisibility() == View.VISIBLE){
                        hideDetails();
                    }
                    else{
                        showDetails(item);
                    }
                }
            });
        }

        private void showDetails(JSONObject item){
            list_item_details.setVisibility(View.VISIBLE);
            // ESTABLECE LISTENER SI LO REQUIERE
            if (onItemClickListener != null) {
                list_item_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClickListener.onItemClick(dataContainer.getFieldCode());
                    }
                });
            }
            //TextView textView = new TextView(context);
            //textView.setText("Detalles");
            //list_item_details.addView(textView);
            if(current != null)
            {
                current.hideDetails();
            }
            current = this;
            Map<String, JSONObject> fields = dataContainer.getFields();
            String key = null;
            for(Iterator<Map.Entry<String, JSONObject>> it = fields.entrySet().iterator(); it.hasNext(); ){
                Map.Entry<String, JSONObject> entry = it.next();
                key = entry.getKey();
                if(item.has(key)){
                    try {
                        if(!fields.get(key).has("hidden") || !fields.get(key).getBoolean("hidden")){
                            TextView itemDetail = new TextView(context);
                            itemDetail.setText(fields.get(key).getString("text") + ": " + item.getString(key));
                            list_item_details.addView(itemDetail);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        private void hideDetails(){
            list_item_details.removeAllViews();
            list_item_details.setVisibility(View.GONE);
        }
    }

    public void updateList(List<JSONObject> list){
        dataContainer.setData(list);
        notifyDataSetChanged();
    }

    public void setDataContainer(JsonDataContainer dataContainer){
        this.dataContainer = dataContainer;
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        holder.hideDetails();
        super.onViewRecycled(holder);
    }
}
