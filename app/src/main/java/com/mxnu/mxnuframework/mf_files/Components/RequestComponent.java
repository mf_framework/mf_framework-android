package com.mxnu.mxnuframework.mf_files.Components;

import android.content.Context;

import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.Models.JsonDataContainer;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class RequestComponent extends ComponentBase {
    protected JSONObject params = null;
    protected String url;
    protected JSONArray requestData;
    public RequestComponent(Context context, JSONObject jsonView, OnCallbackClick onCallackClick, CustomAdapter customAdapter) {
        super(context, jsonView, onCallackClick, customAdapter);
    }

    public void setParams(JSONObject params){
        try {
            jsonView.getJSONObject("ajax").put("params", params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONArray getData(){
        return requestData;
    }

    public abstract void load();

    public void onNetworkChangeStatus(int state) {

    }
}
