package com.mxnu.mxnuframework.mf_files;

/**
 * Created by manu2 on 03/01/2018.
 */

public interface OnCallbackClick {
    void onCallbackClick(int id, Object message);
}
