package com.mxnu.mxnuframework.mf_files.Layouts;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.mxnu.mxnuframework.R;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manuel on 31/01/18.
 */

public class MFLinearLayout extends LayoutBasic {
    public MFLinearLayout(Context context, JSONObject jsonView, OnCallbackClick onCallbackClick, CustomAdapter customAdapter) {
        super(context, jsonView, onCallbackClick, customAdapter);
        this.view = new LinearLayout(context);
        config();
        setup();
    }

    private void config() {
        String orientation = "vertical";
        try{
            if(jsonView.has("orientation")){
                orientation = jsonView.getString("orientation");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ((LinearLayout)view).setOrientation(
                (orientation.equals("vertical") ? LinearLayout.VERTICAL : LinearLayout.HORIZONTAL)
        );
    }

    @Override
    protected void setup() {
        super.setup();
    }

    @Override
    public View createView() {
        ScrollView scrollView = new ScrollView(this.context);
        scrollView.setLayoutParams(view.getLayoutParams());
        scrollView.addView(view);
        return scrollView;
    }
}
