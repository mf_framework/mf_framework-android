package com.mxnu.mxnuframework.mf_files.Util;

/**
 * Created by manuel on 4/02/18.
 */

public interface IRestResult {
    void onSuccess(int code, String response);
    void onError(int code, String error);
}
