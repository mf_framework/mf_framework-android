package com.mxnu.mxnuframework.mf_files.Components;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.mxnu.mxnuframework.mf_files.Adapters.ComboAdapter;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.Models.JsonDataContainer;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.Ajax;
import com.mxnu.mxnuframework.mf_files.Util.IRestResult;
import com.mxnu.mxnuframework.mf_files.Util.MF;
import com.mxnu.mxnuframework.mf_files.Util.RestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manu2 on 08/02/2018.
 */

public class MFcombo extends RequestComponent implements IRestResult, AdapterView.OnItemSelectedListener{
    private ComboAdapter adapter;
    private static String TAG = "MFCOMBO";
    private static final int DATA_RESPONSE = 0;

    public MFcombo(Context context, JSONObject jsonView, OnCallbackClick onCallbackClick, CustomAdapter customAdapter) {
        super(context, jsonView, onCallbackClick, customAdapter);
        this.view = new Spinner(context);
        //this.view = new View(context);
        setup();
    }

    protected void setup() {
        super.setup();
        adapter = new ComboAdapter(context, android.R.layout.simple_spinner_item);
        ((Spinner)view).setAdapter(adapter);
        JSONObject data_container = null;
        try{
            if(jsonView.has("data_container")){
                data_container = jsonView.getJSONObject("data_container");
            }
            if(jsonView.has("events")){
                ((Spinner)this.view).setOnItemSelectedListener(this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(data_container != null){
            fillMfCombo(data_container);
        }
    }

    private void fillMfCombo(JSONObject data_container) {
        JsonDataContainer dataContainer = new JsonDataContainer();
        List<JSONObject> dataList = new ArrayList<>();
        try{
            List<JSONObject> fieldList = new ArrayList<>();
            JSONArray fields = data_container.getJSONArray("fields");
            for (int i = 0; i < fields.length(); i++){
                fieldList.add(fields.getJSONObject(i));
            }
            dataContainer.setFields(fieldList);
            if(data_container.has("code")){
                dataContainer.setFieldCode(data_container.getString("code"));
            }
            if(data_container.has("name")){
                dataContainer.setFieldName(data_container.getString("name"));
            }
            adapter.setDataContainer(dataContainer);

            if(data_container.has("data")){
                JSONArray data = data_container.getJSONArray("data");
                for (int i = 0; i < data.length(); i++){
                    dataList.add(data.getJSONObject(i));
                }
            }
            if(data_container.has("ajax")){
                JSONObject ajax = data_container.getJSONObject("ajax");
                String url = data_container.getJSONObject("ajax").getString("url");
                this.url = url;
                if(data_container.getJSONObject("ajax").has("params")){
                    this.params = data_container.getJSONObject("ajax").getJSONObject("params");
                }
                else{
                    this.params = null;
                }
                if(!ajax.has("autoLoad") || ajax.getBoolean("autoLoad")){
                    load();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter.updateList(dataList);
    }

    @Override
    public void load() {
        RestClient client = new RestClient();
        if(params != null){
            client.execute(url, Ajax.getParams(params, this), DATA_RESPONSE, this);
        }
        else{
            client.execute(url, DATA_RESPONSE, this);
        }
    }

    @Override
    public void onSuccess(int code, String response) {
        switch (code){
            case DATA_RESPONSE:
                List<JSONObject> dataList = new ArrayList<>();
                try{
                    JSONObject res = new JSONObject(response);
                    JSONArray data = res.getJSONArray("data");
                    for (int i = 0; i < data.length(); i++){
                        dataList.add(data.getJSONObject(i));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter.updateList(dataList);
                break;
        }
    }

    @Override
    public void onError(int code, String error) {
        Snackbar.make(this.view, error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public String getValue() {
        String value = (String)((Spinner)this.view).getSelectedItem();
        return value;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int ii, long ll) {
        try {
            JSONArray onSelect = jsonView.getJSONObject("events").getJSONArray("onSelect");
            for (int i = 0; i < onSelect.length(); i++) {
                JSONObject action = onSelect.getJSONObject(i);
                MF.doAction(this, action.getString("action"), action);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
