package com.mxnu.mxnuframework.mf_files.Components;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.ObjectResolver;
import com.mxnu.mxnuframework.mf_files.Util.Util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manuel on 30/01/18.
 */

public class ComponentBase {
    protected Context context;
    protected JSONObject jsonView;
    protected View view;
    protected ComponentBase parent = null;
    protected String name;
    public OnCallbackClick onCallackClick;
    protected CustomAdapter customAdapter;
    private static int _number_of_instances = 1;

    public ComponentBase getParent() {
        return parent;
    }

    public void setParent(ComponentBase parent) {
        this.parent = parent;
    }

    private static String TAG = "COMPONENTBASE";

    public ComponentBase(Context context, JSONObject jsonView, OnCallbackClick onCallackClick, CustomAdapter customAdapter) {
        this.onCallackClick = onCallackClick;
        this.context = context;
        this.jsonView = jsonView;
        this.customAdapter = customAdapter;
        setupName();
    }

    private void setupName() {
        if(jsonView.has("name")){
            try {
                this.name = jsonView.getString("name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            this.name = "component_" + _number_of_instances++;
        }
    }

    protected void setup(){
        setupLayout();
        view.setTag(name);
    }

    protected void setupLayout(){
        ViewGroup.LayoutParams layoutParams;
        String width = "wrap";
        String height = "wrap";
        String color = null;
        String padding = null;
        String layoutType = "LinearLayout";
        double flex = 0.0;
        boolean hidden = false;
        try{
            if(jsonView.has("width")){
                width = jsonView.getString("width");
            }
            if(jsonView.has("height")){
                height = jsonView.getString("height");
            }
            if(jsonView.has("color")){
                color = jsonView.getString("color");
            }
            if(jsonView.has("gravity")){
                layoutType = "RelativeLayout";
            }
            if(jsonView.has("layout_gravity")){
                layoutType = "LinearLayout";
            }
            if(jsonView.has("padding")){
                padding = jsonView.getString("padding");
            }
            if(jsonView.has("hidden")){
                hidden = jsonView.getBoolean("hidden");
            }
            if(jsonView.has("flex")){
                flex = jsonView.getDouble("flex");
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        layoutParams = (ViewGroup.LayoutParams) ObjectResolver.getLayoutInstance(
                layoutType,
                (width.equals("match") ? ViewGroup.LayoutParams.MATCH_PARENT :
                        width.equals("wrap") ?
                                ViewGroup.LayoutParams.WRAP_CONTENT :
                                Integer.parseInt(width)
                ),
                (height.equals("match") ? ViewGroup.LayoutParams.MATCH_PARENT :
                        height.equals("wrap") ?
                                ViewGroup.LayoutParams.WRAP_CONTENT :
                                Integer.parseInt(height)
                ),
                jsonView
        );
        if(layoutParams != null){
            if(flex != 0.0){
                WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                layoutParams.width = (int)Math.floor(size.x * flex);
            }
            view.setLayoutParams(layoutParams);
        }
        if(color != null) {
            view.setBackgroundColor(Color.parseColor(color));
        }
        if(padding != null){
            int[] paddingParams = Util.getMarginParams(padding);
            view.setPadding(paddingParams[0], paddingParams[1], paddingParams[2], paddingParams[3]);
        }
        view.setVisibility(hidden ? View.GONE : View.VISIBLE);
    }

    public String getName() {
        return name;
    }

    public ComponentBase getRoot(){
        ComponentBase current = this;
        while (current.parent != null){
            current = current.parent;
        }
        return current;
    }

    public String getValue(){
        return null;
    }

    public View createView(){
        return view;
    }

    public Context getContext() {
        return context;
    }
}
