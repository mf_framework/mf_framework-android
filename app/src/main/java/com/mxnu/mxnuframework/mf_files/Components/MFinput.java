package com.mxnu.mxnuframework.mf_files.Components;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.view.LayoutInflater;
import android.widget.EditText;

import com.mxnu.mxnuframework.R;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manu2 on 09/02/2018.
 */

public class MFinput extends ComponentBase {
    private EditText editText;
    private TextInputLayout textInputLayout;
    private static String TAG = "INPUT";

    public MFinput(Context context, JSONObject jsonView, OnCallbackClick onCallbackClick, CustomAdapter customAdapter) {
        super(context, jsonView, onCallbackClick, customAdapter);
        this.view = LayoutInflater.from(context).inflate(R.layout.mf_input, null, false);
        editText = this.view.findViewById(R.id.txtInput);
        editText.setSingleLine();
        textInputLayout = this.view.findViewById(R.id.textInputLayout);
        setup();
    }

    @Override
    protected void setup() {
        super.setup();
        String placeholder = null;
        String input_type = null;
        String textColor = null;
        String hintColor = null;
        try{
            if(jsonView.has("placeholder")){
                placeholder = jsonView.getString("placeholder");
            }
            if(jsonView.has("input_type")){
                input_type = jsonView.getString("input_type");
            }
            if(jsonView.has("textColor")){
                textColor = jsonView.getString("textColor");
            }
            if(jsonView.has("hintColot")){
                hintColor = jsonView.getString("hintColor");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(placeholder != null){
            textInputLayout.setHint(placeholder);
        }
        if(textColor != null){
            editText.setTextColor(Color.parseColor(textColor));
            editText.setLinkTextColor(Color.parseColor(textColor));

        }
        if(hintColor != null){
            textInputLayout.getEditText().setHintTextColor(Color.parseColor(hintColor));
        }
        if(input_type != null){
            switch (input_type){
                case "password":
                    editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
                case "number":
                    editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                    break;
            }
        }
    }

    @Override
    public String getValue(){
        return editText.getText().toString();
    }
}
