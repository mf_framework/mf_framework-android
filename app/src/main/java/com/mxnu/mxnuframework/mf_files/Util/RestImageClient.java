package com.mxnu.mxnuframework.mf_files.Util;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by manuel on 6/03/18.
 */

public class RestImageClient {
    private IRestImageResult iRestImageClient;
    private int code;
    private static class SingletonHolder{
        public static RestImageClient INSTANCE = new RestImageClient();
    }
    public static RestImageClient getInstance(){
        return SingletonHolder.INSTANCE;
    }
    public void execute(int code, String url, IRestImageResult iRestImageClient) {
        this.iRestImageClient = iRestImageClient;
        this.code = code;
        new ImageGet().execute(new String[]{url});
    }
    private class ImageGet extends AsyncTask<String, Drawable, Drawable> {

        @Override
        protected Drawable doInBackground(String... params) {
            try {
                URL urlService = new URL(params[0]);
                HttpURLConnection service = (HttpURLConnection) urlService.openConnection();
                if (params.length > 2) {
                    service.setRequestMethod("POST");
                    String data = params[2];
                    service.setDoOutput(true);
                    OutputStream outputStream = new BufferedOutputStream(service.getOutputStream());
                    outputStream.write(data.getBytes());
                    outputStream.flush();
                    outputStream.close();
                }
                BufferedReader br = null;
                StringBuilder sb = new StringBuilder();
                if (service.getResponseCode() == 200) {
                    Drawable drawable = Drawable.createFromStream(service.getInputStream(), "src name");
                    return drawable;
                }
            }
            catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            //super.onPostExecute(drawable);
            if (drawable == null){
                iRestImageClient.onError(code, "Error");
            }
            else {
                iRestImageClient.onSuccess(code, drawable);
            }
        }
    }
}
