package com.mxnu.mxnuframework.mf_files.Components;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.mxnu.mxnuframework.R;
import com.mxnu.mxnuframework.mf_files.Adapters.ListAdapter;
import com.mxnu.mxnuframework.mf_files.Adapters.OnItemListClickListener;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.Models.JsonDataContainer;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.Ajax;
import com.mxnu.mxnuframework.mf_files.Util.IRestResult;
import com.mxnu.mxnuframework.mf_files.Util.MF;
import com.mxnu.mxnuframework.mf_files.Util.RestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manuel on 30/01/18.
 */

public class MFlist extends RequestComponent implements IRestResult, OnItemListClickListener {
    private ListAdapter adapter;
    private static String TAG = "MFLIST";
    private static final int DATA_RESPONSE = 0;

    @Override
    protected void setup() {
        super.setup();
        setupAdapter();
        JSONObject data_container = null;
        try{
            if(jsonView.has("data_container")){
                data_container = jsonView.getJSONObject("data_container");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(data_container != null){
            fillMfList(data_container);
        }
    }

    private void fillMfList(JSONObject data_container) {
        JsonDataContainer dataContainer = new JsonDataContainer();
        List<JSONObject> dataList = new ArrayList<>();
        try{
            List<JSONObject> fieldList = new ArrayList<>();
            JSONArray fields = data_container.getJSONArray("fields");
            for (int i = 0; i < fields.length(); i++){
                fieldList.add(fields.getJSONObject(i));
            }
            dataContainer.setFields(fieldList);
            if(data_container.has("code")){
                dataContainer.setFieldCode(data_container.getString("code"));
            }
            if(data_container.has("name")){
                dataContainer.setFieldName(data_container.getString("name"));
            }
            adapter.setDataContainer(dataContainer);

            if(data_container.has("data")){
                JSONArray data = data_container.getJSONArray("data");
                for (int i = 0; i < data.length(); i++){
                    dataList.add(data.getJSONObject(i));
                }
            }
            if(data_container.has("ajax")){
                JSONObject ajax = data_container.getJSONObject("ajax");
                String url = data_container.getJSONObject("ajax").getString("url");
                this.url = url;
                if(data_container.getJSONObject("ajax").has("params")){
                    this.params = data_container.getJSONObject("ajax").getJSONObject("params");
                }
                else{
                    this.params = null;
                }
                if(!ajax.has("autoLoad") || ajax.getBoolean("autoLoad")){
                    load();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter.updateList(dataList);
    }

    public MFlist(Context context, JSONObject jsonView, OnCallbackClick onCallbackClick, CustomAdapter customAdapter) {
        super(context, jsonView, onCallbackClick, customAdapter);
        this.view = LayoutInflater.from(context).inflate(R.layout.mf_list, null, false);
        setup();
    }

    @Override
    public View createView() {
        if(jsonView.has("title")){
            try {
                ((TextView)view.findViewById(R.id.txtTitle)).setText(jsonView.getString("title"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return view;
        }
        else{
            return super.createView();
        }
    }

    private void setupAdapter() {
        adapter = new ListAdapter(context, customAdapter, name);
        adapter.setOnItemClickListener(this);
        ((RecyclerView)this.view.findViewById(R.id.recyclerViewList)).setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        ((RecyclerView)this.view.findViewById(R.id.recyclerViewList)).setAdapter(adapter);
    }

    @Override
    public void onSuccess(int code, String response) {
        switch (code){
            case DATA_RESPONSE:
                List<JSONObject> dataList = new ArrayList<>();
                try{
                    JSONObject res = new JSONObject(response);
                    JSONArray data = res.getJSONArray("data");
                    for (int i = 0; i < data.length(); i++){
                        dataList.add(data.getJSONObject(i));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter.updateList(dataList);
                break;
        }
    }

    @Override
    public void setParams(JSONObject params) {
        this.params = params;
    }

    @Override
    public void load() {
        RestClient client = new RestClient();
        if(params != null){
            client.execute(url, Ajax.getParams(params, this), DATA_RESPONSE, this);
        }
        else{
            client.execute(url, DATA_RESPONSE, this);
        }
    }

    @Override
    public void onError(int code, String error) {
        Snackbar.make(this.view, "error", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(String code) {
        try {
            if (jsonView.has("events") && jsonView.getJSONObject("events").has("onClick")) {
                JSONArray onClickActions = jsonView.getJSONObject("events").getJSONArray("onClick");
                for (int i = 0; i < onClickActions.length(); i++) {
                    JSONObject action = onClickActions.getJSONObject(i);
                    MF.doAction(this, action.getString("action"), action);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
