package com.mxnu.mxnuframework.mf_files;

public class CustomAdapterEnum {
    public static final int TABLE = 0;
    public static final int LIST = 1;
    public static final int VERTICAL = 2;
    public static final int HORIZONTAL = 4;
}
