package com.mxnu.mxnuframework.mf_files.Util;

import android.graphics.drawable.Drawable;

/**
 * Created by manuel on 6/03/18.
 */

public interface IRestImageResult {
    void onSuccess(int code, Drawable drawable);
    void onError(int code, String error);
}
