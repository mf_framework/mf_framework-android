package com.mxnu.mxnuframework.mf_files;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mxnu.mxnuframework.mf_files.Components.ComponentBase;
import com.mxnu.mxnuframework.mf_files.Util.ObjectResolver;
import com.mxnu.mxnuframework.mf_files.Util.Sessions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Created by manu2 on 02/01/2018.
 */

public class MxnuFramework extends AppCompatActivity {
    private static String TAG = "MXNUFRAMEWORK";
    private JSONObject jsonView;
    private View MainContainer;
    private OnCallbackClick onCallbackClick;
    private CustomAdapter customAdapter;
    public MxnuFramework() {
        try {
            jsonView = new JSONObject("{\"mf-type\":\"RelativeLayout\",\"childs\":[{\"mf-type\":\"label\",\"value\":\"MxnuFramework\"}]}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void setOnCallbackClick(OnCallbackClick onCallbackClick){
        this.onCallbackClick = onCallbackClick;
    }
    public void setCustomAdapter(CustomAdapter customAdapter){
        this.customAdapter = customAdapter;
    }
    public MxnuFramework(JSONObject view){
        jsonView = view;
    }
    public void loadView(JSONObject view){
        jsonView = view;
    }
    public void loadView(int resource){
        InputStream is = getResources().openRawResource(resource);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        JSONObject jsonView = null;
        try {
            jsonView = new JSONObject(writer.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("MxnuFramework", e.getMessage());
        }
        loadView(jsonView);
        try {
            render();
        } catch (JSONException e) {
            Log.e("MxnuFramework", "onRender()" + e.getMessage());
            e.printStackTrace();
        }
    }
    private void render() throws JSONException{
        //View view = createLayout(jsonView);
        ComponentBase view = (ComponentBase) ObjectResolver
                .getViewInstance(
                        //"LinearLayout",
                        jsonView.getString("mf-type"),
                        this,
                        jsonView,
                        onCallbackClick,
                        customAdapter
                );
        View main = view.createView();
        if(main == null){
            Log.e(TAG, "Vista nula");
            return;
        }
        MainContainer = main;
        if(view != null){
            setContentView(MainContainer);
        }
    }
    private View createComponent(JSONObject jsonComponent) throws JSONException{
        String type = jsonComponent.getString("mf-type");
        View componentView = null;
        switch (type){
            case "text":
                TextView textView = new TextView(this);
                if(jsonComponent.has("value")){
                    textView.setText(jsonComponent.getString("value"));
                }
                componentView = textView;
                break;
            case "button":
                Button button = new Button(this);
                if(jsonComponent.has("text")){
                    button.setText(jsonComponent.getString("text"));
                }
                if(jsonComponent.has("actions")){
                    final JSONObject actions = jsonComponent.getJSONObject("actions");
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                JSONArray names = actions.names();
                                for (int i = 0; i < names.length(); i++) {
                                    String action = names.getString(i);
                                    JSONObject params;
                                    switch (action) {
                                        case "alert":
                                            String message = actions.getString(action);
                                            Snackbar.make(MainContainer, message, Snackbar.LENGTH_LONG).show();
                                            break;
                                        case "callback":
                                            params = actions.getJSONObject(action);
                                            if(onCallbackClick != null) {
                                                onCallbackClick.onCallbackClick(params.getInt("id"), null);
                                            }
                                            break;
                                        case "launch":
                                            params = actions.getJSONObject(action);
                                            if(params.has("activity")){
                                                //Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                Intent intent = new Intent(getPackageName() +
                                                    "." +
                                                    params.getString("activity")
                                                );
                                                /*intent.addFlags(
                                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                                    Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                                    Intent.FLAG_ACTIVITY_NEW_TASK
                                                );*/
                                                startActivity(intent);
                                            }
                                            break;
                                    }
                                }
                            }
                            catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                componentView = button;
                break;
            case "TextBox":
            case "input":
            case "Entry":
                EditText editText = new EditText(this);
                if(jsonComponent.has("placeholder")){
                    editText.setHint(jsonComponent.getString("placeholder"));
                }
                componentView = editText;
                break;
        }
        return componentView;
    }
    private View createLinearLayout(JSONObject jsonLayout) throws JSONException{
        LinearLayout linearLayout = new LinearLayout(this);
        String width = "WRAP";
        String height = "WRAP";
        if(jsonLayout.has("width")) {
            width = jsonLayout.getString("width");
            Log.e("createLinearLayout", "has_width" + width);
        }
        if(jsonLayout.has("height")) {
            height = jsonLayout.getString("height");
            Log.e("createLinearLayout", "has-heigth" + height);
        }
        LinearLayout.LayoutParams LLparams = new LinearLayout.LayoutParams(
            (width.equals("MATCH") ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.WRAP_CONTENT),
            (height.equals("MATCH") ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.WRAP_CONTENT)
        );
        if(jsonLayout.has("orientation")){
            String orientation = jsonLayout.getString("orientation");
            switch (orientation){
                case "vertical":
                    linearLayout.setOrientation(LinearLayout.VERTICAL);
                    break;
                case "horizontal":
                    linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            }
        }
        else {
            linearLayout.setOrientation(LinearLayout.VERTICAL);
        }
        linearLayout.setLayoutParams(LLparams);

        if(jsonLayout.has("childs")){
            JSONArray childs = jsonLayout.getJSONArray("childs");
            for(int i = 0; i < childs.length(); i++){
                JSONObject jsonComponent = childs.getJSONObject(i);
                View component = createComponent(jsonComponent);
                if(component != null){
                    width = "WRAP";
                    height = "WRAP";
                    Log.e("createComponent", jsonComponent.getString("mf-type"));
                    if(jsonComponent.has("width")) {
                        width = jsonComponent.getString("width");
                        Log.e("createLinearLayoutChild", "has-width" + width);
                    }
                    if(jsonComponent.has("height")) {
                        height = jsonComponent.getString("height");
                        Log.e("createLinearLayoutChild", "has-heigth" + height);
                    }
                    LinearLayout.LayoutParams LLCParams = new LinearLayout.LayoutParams(
                        (width.equals("MATCH") ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.WRAP_CONTENT),
                        (height.equals("MATCH") ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.WRAP_CONTENT)
                    );
                    if(jsonComponent.has("margin")){
                        int margin = jsonComponent.getInt("margin");
                        LLCParams.setMargins(margin, margin, margin, margin);
                    }
                    component.setLayoutParams(LLCParams);
                    linearLayout.addView(component);
                }
            }
        }

        return linearLayout;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Sessions.getInstance().setContext(getApplicationContext()).build();
        try {
            render();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}