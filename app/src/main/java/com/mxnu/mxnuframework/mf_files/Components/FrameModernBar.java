package com.mxnu.mxnuframework.mf_files.Components;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.ObjectResolver;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manu2 on 08/02/2018.
 */

public class FrameModernBar extends Fragment {
    JSONObject jsonView;
    OnCallbackClick onCallbackClick;
    private CustomAdapter customAdapter;

    public void setOnCallbackClick(OnCallbackClick onCallbackClick){
        this.onCallbackClick = onCallbackClick;
    }

    public void setCustomAdapter(CustomAdapter customAdapter){
        this.customAdapter = customAdapter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            jsonView = new JSONObject(getArguments().getString("jsonView"));
            ComponentBase view = (ComponentBase) ObjectResolver
                    .getViewInstance(
                            //"LinearLayout",
                            jsonView.getString("mf-type"),
                            getContext(),
                            jsonView,
                            onCallbackClick,
                            customAdapter
                    );
            return view.createView();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
