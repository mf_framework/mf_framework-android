package com.mxnu.mxnuframework.mf_files.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manuel on 22/03/18.
 */

public class Sessions {
    private static String TAG = "SESSIONS";
    private static final String SHAREDPREFERENCES_KEY = "MF_SHAREDPREFERENCES";
    private SharedPreferences sharedPreferences;
    private Context context;
    public Sessions setContext(Context context){
        this.context = context;
        return this;
    }
    public void build(){
        this.sharedPreferences = this.context.getSharedPreferences(SHAREDPREFERENCES_KEY, Context.MODE_PRIVATE);
    }
    private static class SingletonHolder{
        public static final Sessions INSTANCE = new Sessions();
    }
    public static Sessions getInstance(){
        return SingletonHolder.INSTANCE;
    }
    public void write(String key, JSONObject value){
        sharedPreferences.edit().putString(key, value.toString()).apply();
    }
    public JSONObject read(String key) {
        if(!sharedPreferences.contains(key)){
            return null;
        }
        String jsonString = sharedPreferences.getString(key, "{}");
        JSONObject jsonObject = null;
        try{
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
    public void clear(){
        sharedPreferences.edit().clear().apply();
    }
}
