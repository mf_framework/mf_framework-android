package com.mxnu.mxnuframework.mf_files;


import android.support.v7.widget.RecyclerView;

import com.mxnu.mxnuframework.mf_files.Adapters.ListAdapter;

public interface CustomAdapter {
    void onBind(String name, RecyclerView.ViewHolder holder, int position, int type);
}
