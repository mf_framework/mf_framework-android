package com.mxnu.mxnuframework.mf_files.Adapters;

public interface OnItemListClickListener {
    void onItemClick(String code);
}
