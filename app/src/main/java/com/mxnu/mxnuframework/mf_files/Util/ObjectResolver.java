package com.mxnu.mxnuframework.mf_files.Util;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by manuel on 31/01/18.
 */

public class ObjectResolver {
    private static String TAG = "OBJECTRESOLVER";
    public static Object getViewInstance(String className, Context context, JSONObject jsonView, OnCallbackClick onCallbackClick, CustomAdapter customAdapter){
        Object instance = null;
        try {
            String folder = (className.contains("Layout") ? "Layouts" : "Components");
            className = "com.mxnu.mxnuframework" + ".mf_files." + folder + ".MF" + className;
            //className = context.getPackageName() + ".mf_files." + folder + ".MF" + className;
            Class<?> clase = Class.forName(className);
            Constructor<?> constructor = clase.getConstructor(Context.class, JSONObject.class, OnCallbackClick.class, CustomAdapter.class);
            instance = constructor.newInstance(context, jsonView, onCallbackClick, customAdapter);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return instance;
    }
    public static Object getLayoutInstance(String className, int width, int heigth, JSONObject jsonView){
        Object instance = null;
        switch (className){
            case "LinearLayout":
                instance = new LinearLayout.LayoutParams(width, heigth);
                String layout_gravity = null;
                try {
                    if(jsonView.has("layout_gravity")){
                        layout_gravity = jsonView.getString("layout_gravity");
                    }
                    if(jsonView.has("margin")){
                        String margin = jsonView.getString("margin");
                        int[] margin_lengths = Util.getMarginParams(margin);
                        ((LinearLayout.LayoutParams)instance).setMargins(
                                margin_lengths[0],
                                margin_lengths[1],
                                margin_lengths[2],
                                margin_lengths[3]
                        );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(layout_gravity != null){
                    ((LinearLayout.LayoutParams)instance).gravity = (layout_gravity.equals("center_horizontal") ?
                            Gravity.CENTER : Gravity.NO_GRAVITY);
                }
                break;
            case "RelativeLayout":
                instance = new RelativeLayout.LayoutParams(width, heigth);
                String gravity = null;
                try{
                    if(jsonView.has("gravity")){
                        gravity = jsonView.getString("gravity");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(gravity != null){
                    switch (gravity){
                        case "center":
                            Log.e(TAG, "center");
                            ((RelativeLayout.LayoutParams)instance).addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                            break;
                    }
                }
                break;
        }
        return instance;
    }
}
