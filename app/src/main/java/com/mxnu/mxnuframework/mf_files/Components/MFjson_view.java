package com.mxnu.mxnuframework.mf_files.Components;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.Ajax;
import com.mxnu.mxnuframework.mf_files.Util.IRestResult;
import com.mxnu.mxnuframework.mf_files.Util.ObjectResolver;
import com.mxnu.mxnuframework.mf_files.Util.RestClient;
import com.mxnu.mxnuframework.mf_files.Util.Sessions;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manu2 on 08/02/2018.
 */

public class MFjson_view extends ComponentBase implements IRestResult {

    private static String TAG = "MFJSONVIEW";
    private static final int DATA_RESPONSE = 0;

    public MFjson_view(Context context, JSONObject jsonView, OnCallbackClick onCallbackClick, CustomAdapter customAdapter) {
        super(context, jsonView, onCallbackClick, customAdapter);
        this.view = new ProgressBar(context);
        setup();

    }

    public void setup()
    {
        super.setup();
        JSONObject ajax = null;
        boolean llCache = false;
        String lcName = null;
        try{
            if(jsonView.has("ajax")){
                ajax = jsonView.getJSONObject("ajax");
            }
            if (jsonView.has("cache")) {
                llCache = jsonView.getBoolean("cache");
            }
            if (jsonView.has("name")) {
                lcName = jsonView.getString("name");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (llCache && lcName != null) {
            String lcSesNam = String.format("SESSION_%s", lcName);
            JSONObject viewJson = Sessions.getInstance().read(lcSesNam);
            if (viewJson == null && ajax != null) {
                fillMfjson_view(ajax);
            }
            else {
                this.view = createViewFromJson(viewJson).createView();
            }
        }
        else if (ajax != null){
            fillMfjson_view(ajax);
        }
    }

    private void fillMfjson_view(JSONObject ajax) {
        try
        {
            if(ajax.has("url")){
                RestClient client = new RestClient();
                String url = ajax.getString("url");
                if (ajax.has("params")) {
                    String params = Ajax.getParams(ajax.getJSONObject("params"), this);
                    client.execute(url, params, DATA_RESPONSE, this);
                }
                else {
                    client.execute(url, DATA_RESPONSE,this);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void onSuccess(int code, String response) {
        switch (code) {
            case DATA_RESPONSE:
                try {
                    JSONObject responseObj = new JSONObject(response);
                    if(!responseObj.getBoolean("success")){
                        break;
                    }
                    String responseStr = responseObj.getString("response");
                    JSONObject res = new JSONObject(responseStr);

                    ComponentBase new_view = createViewFromJson(res);
                    if (new_view == null) {
                        return;
                    }
                    // import new view to old
                    View current = this.getRoot().view.findViewWithTag(this.name);
                    ViewGroup parent = (ViewGroup) current.getParent();
                    int index = parent.indexOfChild(current);
                    parent.removeView(current);
                    parent.addView(new_view.createView(), index);

                    // IF CACHE OPTION ENABLE, SAVE
                    if (jsonView.has("cache") && jsonView.getBoolean("cache" ) && jsonView.has("name")) {
                        String lcSesNam = String.format("SESSION_%s", jsonView.getString("name"));
                        Sessions.getInstance().write(lcSesNam, res);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private ComponentBase createViewFromJson(JSONObject viewJson) {
        try {
            ComponentBase view2 = (ComponentBase) ObjectResolver.getViewInstance(
                    viewJson.getString("mf-type"),
                    this.context,
                    viewJson,
                    onCallackClick,
                    customAdapter
            );
            /*View current = this.getRoot().view.findViewWithTag(this.name);
            ViewGroup parent = (ViewGroup) current.getParent();
            int index = parent.indexOfChild(current);
            parent.removeView(current);
            parent.addView(view2.createView(), index);*/
            return view2;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onError(int code, String error) {
        Snackbar.make(this.view, error, Snackbar.LENGTH_LONG).show();
    }
}

