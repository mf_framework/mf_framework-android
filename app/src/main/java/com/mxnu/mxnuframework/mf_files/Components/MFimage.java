package com.mxnu.mxnuframework.mf_files.Components;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.mxnu.mxnuframework.R;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.IRestImageResult;
import com.mxnu.mxnuframework.mf_files.Util.RestImageClient;
import com.mxnu.mxnuframework.mf_files.Util.Util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manuel on 3/03/18.
 */

public class MFimage extends ComponentBase implements IRestImageResult {
    private ImageView ImageMF;
    private ProgressBar imageProgressBar;

    public MFimage(Context context, JSONObject jsonView, OnCallbackClick onCallackClick, CustomAdapter customAdapter) {
        super(context, jsonView, onCallackClick, customAdapter);
        this.view = new ImageView(context);

        this.view = LayoutInflater.from(context).inflate(R.layout.mf_image, null, false);
        ImageMF = this.view.findViewById(R.id.ImageMF);
        //ImageMF.setAdjustViewBounds(true);
        imageProgressBar = this.view.findViewById(R.id.imageProgressBar);
        setup();

    }


    @Override
    protected void setup() {
        super.setup();
        try{
            boolean hiddenProgress = false;
            if(jsonView.has("hiddenProgress")){
                hiddenProgress = jsonView.getBoolean("hiddenProgress");
            }
            if(jsonView.has("bitimg")){
                String bitimg = jsonView.getString("bitimg");
                ImageMF.setImageResource(Util.getResId(context, bitimg));
            }
            if(jsonView.has("url")){
                String url = jsonView.getString("url");
                RestImageClient client = RestImageClient.getInstance();
                ImageMF.setVisibility(View.GONE);
                if(!hiddenProgress){
                    imageProgressBar.setVisibility(View.VISIBLE);
                }
                client.execute(0, url, this);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(int code, Drawable drawable) {
        imageProgressBar.setVisibility(View.GONE);
        ImageMF.setImageDrawable(drawable);
        ImageMF.requestLayout();
        ImageMF.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(int code, String error) {
        imageProgressBar.setVisibility(View.GONE);
        //set image reload
    }
}
