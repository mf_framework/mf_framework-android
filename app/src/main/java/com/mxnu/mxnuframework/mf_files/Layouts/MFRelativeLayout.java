package com.mxnu.mxnuframework.mf_files.Layouts;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.mxnu.mxnuframework.mf_files.Components.ComponentBase;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manu2 on 08/02/2018.
 */

public class MFRelativeLayout extends LayoutBasic {
    public MFRelativeLayout(Context context, JSONObject jsonLayout, OnCallbackClick onCallbackClick, CustomAdapter customAdapter) {
        super(context, jsonLayout, onCallbackClick, customAdapter);
        this.view = new RelativeLayout(context);
        setup();
    }
}
