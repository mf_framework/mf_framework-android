package com.mxnu.mxnuframework;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.mxnu.mxnuframework.mf_files.Adapters.ColumnAdapter;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.CustomAdapterEnum;
import com.mxnu.mxnuframework.mf_files.MxnuFramework;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.Sessions;

import org.json.JSONArray;
import org.json.JSONException;

public class MainActivity extends MxnuFramework implements OnCallbackClick, CustomAdapter {
    private static final String TAG = "MAINACTIVITY";
    public MainActivity() {
        super();
        setOnCallbackClick(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCustomAdapter(this);
        loadView(R.raw.main_view);
        getSupportActionBar().setCustomView(R.layout.title_app);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        TextView titleText = findViewById(R.id.txt_title);
        Sessions sessions = Sessions.getInstance();
        try {
            String nombre = sessions.read("LOGIN").getString("CNOMBRE");
            //String lastName = nombre.substring(0, nombre.indexOf(' '));
            //String firstName = nombre.substring(nombre.indexOf(',') + 2, nombre.indexOf(' ', nombre.indexOf(',') + 2));
            //titleText.setText("UCSM: " + firstName + " " + lastName);
            titleText.setText("UCSM: " + nombre);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //setTitle("Eventos");
    }

    @Override
    public void onCallbackClick(int id, Object message) {
        switch (id){
            case 1:
                JSONArray data = (JSONArray) message;
                Log.e(TAG, data.toString());
                break;
        }
    }

    @Override
    public void onBind(String name, RecyclerView.ViewHolder holder, int position, int type) {
        if(type == (CustomAdapterEnum.TABLE | CustomAdapterEnum.HORIZONTAL)){
            if(position % 2 == 0){
                ((ColumnAdapter.ViewHolder)holder).table_column_item.setBackgroundColor(Color.argb(30, 0, 255, 30));
            }
            else{
                ((ColumnAdapter.ViewHolder)holder).table_column_item.setBackgroundColor(Color.argb(30, 0, 100, 30));
            }
        }
    }
}
