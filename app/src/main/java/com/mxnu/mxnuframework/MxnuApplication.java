package com.mxnu.mxnuframework;

import android.app.Application;
import android.content.Context;

import com.mxnu.mxnuframework.mf_files.Util.Sessions;

/**
 * Created by manu2 on 02/01/2018.
 */

public class MxnuApplication extends Application {
    private String token;
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public Context getMxnuFrameworkContext(){
        return getApplicationContext();
    }
    public MxnuApplication(){

    }
}
