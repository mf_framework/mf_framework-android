package com.mxnu.mxnuframework.Activities.Managers;

public interface IFingerprint {
    void onAuthSuccess();
    void onAuthFailed(FingerprintHandler.FINGERPRINT_ERROR fingerprint_error, String message);
}
