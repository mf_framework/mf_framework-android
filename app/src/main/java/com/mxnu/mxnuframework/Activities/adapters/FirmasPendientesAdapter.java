package com.mxnu.mxnuframework.Activities.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mxnu.mxnuframework.Activities.Entities.Firma;
import com.mxnu.mxnuframework.R;

import java.util.ArrayList;
import java.util.List;

public class FirmasPendientesAdapter extends RecyclerView.Adapter<FirmasPendientesAdapter.ViewHolder> {
    private List<Firma> laFirmas = new ArrayList<>();
    private IFirmaClickListener iFirmaClickListener;

    public FirmasPendientesAdapter(IFirmaClickListener iFirmaClickListener) {
        this.iFirmaClickListener = iFirmaClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View loView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.firma_item, viewGroup, false);
        return new ViewHolder(loView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int p_nIndex) {
        Firma loFirma = laFirmas.get(p_nIndex);
        viewHolder.txtNombre.setText(loFirma.getcNombre());
        viewHolder.txtFechaRecepcion.setText(loFirma.getdRecepc());
        viewHolder.txtNomUni.setText(loFirma.getcNomUni());
        viewHolder.txtDescripcion.setText(loFirma.getcDescri());
        viewHolder.setOnFirmaClickListener(loFirma);
    }

    @Override
    public int getItemCount() {
        return laFirmas.size();
    }

    public void omSetListFirmas(List<Firma> p_aFirmas) {
        this.laFirmas = p_aFirmas;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtFechaRecepcion;
        private TextView txtNombre;
        private TextView txtNomUni;
        private TextView txtDescripcion;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            bind();
        }

        private void bind() {
            txtFechaRecepcion = itemView.findViewById(R.id.txtFechaRecepcion);
            txtNombre = itemView.findViewById(R.id.txtNombre);
            txtNomUni = itemView.findViewById(R.id.txtNomUni);
            txtDescripcion = itemView.findViewById(R.id.txtDescripcion);
        }

        public void setOnFirmaClickListener(final Firma p_oFirma) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    iFirmaClickListener.onFirmaClickListener(p_oFirma);
                }
            });
        }
    }
}
