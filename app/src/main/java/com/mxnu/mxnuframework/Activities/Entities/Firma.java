package com.mxnu.mxnuframework.Activities.Entities;

public class Firma {
    private String cCodAlu;
    private String cNombre;
    private String cUniAca;
    private String cNomUni;
    private String dRecepc;
    private String cNroDni;
    private String cCodTre;
    private String cDescri;
    private String nCanFir;

    public String getcCodAlu() {
        return cCodAlu;
    }

    public void setcCodAlu(String cCodAlu) {
        this.cCodAlu = cCodAlu;
    }

    public String getcNombre() {
        return cNombre;
    }

    public void setcNombre(String cNombre) {
        this.cNombre = cNombre;
    }

    public String getcUniAca() {
        return cUniAca;
    }

    public void setcUniAca(String cUniAca) {
        this.cUniAca = cUniAca;
    }

    public String getcNomUni() {
        return cNomUni;
    }

    public void setcNomUni(String cNomUni) {
        this.cNomUni = cNomUni;
    }

    public String getdRecepc() {
        return dRecepc;
    }

    public void setdRecepc(String dRecepc) {
        this.dRecepc = dRecepc;
    }

    public String getcNroDni() {
        return cNroDni;
    }

    public void setcNroDni(String cNroDni) {
        this.cNroDni = cNroDni;
    }

    public String getcCodTre() {
        return cCodTre;
    }

    public void setcCodTre(String cCodTre) {
        this.cCodTre = cCodTre;
    }

    public String getcDescri() {
        return cDescri;
    }

    public void setcDescri(String cDescri) {
        this.cDescri = cDescri;
    }

    public String getnCanFir() {
        return nCanFir;
    }

    public void setnCanFir(String nCanFir) {
        this.nCanFir = nCanFir;
    }
}
