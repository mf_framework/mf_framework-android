package com.mxnu.mxnuframework.Activities.adapters;

import com.mxnu.mxnuframework.Activities.Entities.Firma;

public interface IFirmaClickListener {
    void onFirmaClickListener(Firma firma);
}
