package com.mxnu.mxnuframework.Activities;

import android.app.KeyguardManager;
import android.content.SharedPreferences;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.mxnu.mxnuframework.Activities.Managers.FingerprintHandler;
import com.mxnu.mxnuframework.Activities.Managers.IFingerprint;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class CActivityBase extends AppCompatActivity {
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private Cipher cipher;
    private FingerprintManager.CryptoObject cryptoObject;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    protected String pcError;
    protected IFingerprint iFingerprint;
    protected Map<String, String> paData = new HashMap<>();

    private static final String lcSession = "UCSMAPPS";
    private static final String KEY_NAME = "ABCDE";

    protected void omConfigureActionBar() {
        /*Toolbar toolbar  = findViewById(R.id.actionBar);
        setSupportActionBar(toolbar);
        setTitle("");*/
    }

    protected void omSaveInSession(String p_cKey, String p_cValue) {
        SharedPreferences loShared = getSharedPreferences(lcSession, MODE_PRIVATE);
        loShared.edit().putString(p_cKey, p_cValue).commit();
    }

    protected void omClearSession() {
        SharedPreferences loShared = getSharedPreferences(lcSession, MODE_PRIVATE);
        loShared.edit().clear().commit();
    }

    protected String omGetValSession(String p_cKey) {
        SharedPreferences loShared = getSharedPreferences(lcSession, MODE_PRIVATE);
        return loShared.getString(p_cKey, null);
    }

    protected boolean setupFingerprint() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
            if (!fingerprintManager.isHardwareDetected()) {
                this.pcError = "NO SE TIENEN PERMISOS PARA ACCEDER A LOS SENSORES";
                return false;
            }
            if (!fingerprintManager.hasEnrolledFingerprints()) {
                this.pcError = "NO SE ENCUENTRA CONFIGURADO EL SENSOR DE HUELLAS";
                return false;
            }
            if (keyguardManager.isKeyguardSecure()) {
                generateKeys();
            }
            if (initCipher()) {
                cryptoObject = new FingerprintManager.CryptoObject(cipher);
                FingerprintHandler helper = new FingerprintHandler(this, iFingerprint);
                helper.startAuth(fingerprintManager, cryptoObject);
                return true;
            }
        }
        else  {
            this.pcError = "NO TIENE SENSOR DE HUELLAS";
            return false;
        }
        return false;
    }

    private boolean initCipher() {
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/" +
                            KeyProperties.BLOCK_MODE_CBC + "/" +
                            KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME, null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void generateKeys() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");

            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            keyStore.load(null);

            keyGenerator.init(new KeyGenParameterSpec.Builder(
                    KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7
                    ).build());
            keyGenerator.generateKey();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
    }
}
