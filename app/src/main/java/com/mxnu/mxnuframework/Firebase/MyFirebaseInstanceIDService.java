package com.mxnu.mxnuframework.Firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.mxnu.mxnuframework.mf_files.Util.Sessions;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        storeToken(refreshedToken);
    }

    private void storeToken(String token) {
        //saving the token on shared preferences
        try {
            JSONObject loJson = new JSONObject();
            loJson.put("token", token);
            Sessions.getInstance().write("token", loJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //SharedPrefManager.getInstance(getApplicationContext()).saveDeviceToken(token);
    }
}
