package com.mxnu.mxnuframework;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.mxnu.mxnuframework.mf_files.Adapters.ColumnAdapter;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.CustomAdapterEnum;
import com.mxnu.mxnuframework.mf_files.MxnuFramework;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.Sessions;

import org.json.JSONArray;
import org.json.JSONException;

public class EventosActivity extends MxnuFramework{
    private static final String TAG = "EVENTOSACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadView(R.raw.eventos_view);
        getSupportActionBar().setCustomView(R.layout.title_app);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        TextView titleText = findViewById(R.id.txt_title);
        Sessions sessions = Sessions.getInstance();
        try {
            String nombre = sessions.read("LOGIN").getString("CNOMBRE");
            //String lastName = nombre.substring(0, nombre.indexOf(' '));
            //String firstName = nombre.substring(nombre.indexOf(',') + 2, nombre.indexOf(' ', nombre.indexOf(',') + 2));
            //titleText.setText("UCSM: " + firstName + " " + lastName);
            titleText.setText("UCSM: " + nombre);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //setTitle("Eventos");
    }
}
