package com.mxnu.mxnuframework;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mxnu.mxnuframework.Activities.CActivityBase;
import com.mxnu.mxnuframework.Activities.Entities.Firma;
import com.mxnu.mxnuframework.Activities.Managers.FingerprintHandler;
import com.mxnu.mxnuframework.Activities.Managers.IFingerprint;
import com.mxnu.mxnuframework.Activities.adapters.FirmasPendientesAdapter;
import com.mxnu.mxnuframework.Activities.adapters.IFirmaClickListener;
import com.mxnu.mxnuframework.mf_files.Util.IRestResult;
import com.mxnu.mxnuframework.mf_files.Util.RestClient;
import com.mxnu.mxnuframework.mf_files.Util.Sessions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FirmasActivity extends CActivityBase implements IRestResult, IFirmaClickListener, IFingerprint, View.OnClickListener {
    private RecyclerView recyclerview_Pendientes;
    private FirmasPendientesAdapter pendientesAdapter;
    private View main_view;
    private AlertDialog fingerprintDialog;
    private static final int GET_FIRMAS = 0;
    private static final int GET_FIRMAR = 1;
    private static final String TAG = "FIRMASACT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setCustomView(R.layout.title_app);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        TextView titleText = findViewById(R.id.txt_title);
        Sessions sessions = Sessions.getInstance();
        try {
            String nombre = sessions.read("LOGIN").getString("CNOMBRE");
            //String lastName = nombre.substring(0, nombre.indexOf(' '));
            //String firstName = nombre.substring(nombre.indexOf(',') + 2, nombre.indexOf(' ', nombre.indexOf(',') + 2));
            //titleText.setText("UCSM: " + firstName + " " + lastName);
            titleText.setText("UCSM: " + nombre);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_firmar);
        bind();
        setupAdapters();
        omLoadPendientes();
    }

    private void omLoadPendientes() {
        try {
            JSONObject loJson = new JSONObject();
            JSONObject loData = new JSONObject();
            loJson.put("Id", "RecuperarFirmasPendientes");
            //loData.put("CCODUSU", omGetValSession("CCODUSU"));
            loData.put("CCODUSU", Sessions.getInstance().read("LOGIN").getString("CCODDOC"));
            loJson.put("paData", loData);
            RestClient loClient = new RestClient();
            loClient.execute("http://apps.ucsm.edu.pe/UCSMMTA/wsApp.php", loJson.toString(), GET_FIRMAS, this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupAdapters() {
        pendientesAdapter = new FirmasPendientesAdapter(this);
        recyclerview_Pendientes.setAdapter(pendientesAdapter);
        recyclerview_Pendientes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private void bind() {
        recyclerview_Pendientes = findViewById(R.id.recyclerview_Pendientes);
        main_view = findViewById(R.id.main_view);
        findViewById(R.id.btnSalir).setOnClickListener(this);
    }

    @Override
    public void onSuccess(int code, String response) {
        switch (code) {
            case GET_FIRMAS:
                List<Firma> laFirmas = new ArrayList<>();
                //GET DATA
                try {
                    JSONArray laDatos = new JSONArray(response);
                    for (int i = 0; i < laDatos.length(); i++) {
                        JSONObject loFirma = laDatos.getJSONObject(i);
                        Firma loTmp = new Firma();
                        loTmp.setcCodTre(loFirma.getString("CCODTRE"));
                        loTmp.setcNombre(loFirma.getString("CNOMBRE"));
                        loTmp.setdRecepc(loFirma.getString("DRECEPC"));
                        loTmp.setcCodAlu(loFirma.getString("CCODALU"));
                        loTmp.setcNomUni(loFirma.getString("CNOMUNI"));
                        loTmp.setcDescri(loFirma.getString("CDESCRI"));
                        laFirmas.add(loTmp);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                pendientesAdapter.omSetListFirmas(laFirmas);
                break;
            case GET_FIRMAR:
                fingerprintDialog.dismiss();
                try {
                    JSONObject loJson = new JSONObject(response);
                    if (loJson.has("ERROR")) {
                        Snackbar.make(main_view, loJson.getString("ERROR"), Snackbar.LENGTH_SHORT).show();
                    }
                    else {
                        Snackbar.make(main_view, "SE FIRMO EL DOCUMENTO", Snackbar.LENGTH_SHORT).show();
                        omLoadPendientes();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Snackbar.make(main_view, "PROBLEMA EN LA RESPUESTA OBTENIDA", Snackbar.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onError(int code, String error) {
        Snackbar.make(main_view, error, Snackbar.LENGTH_SHORT).show();
        if (code == GET_FIRMAS) {
            List<Firma> laFirmas = new ArrayList<>();
            //DEMO
            Firma firma = new Firma();
            firma.setcCodAlu("2014202221");
            firma.setcNombre("BARRIGA/SOTO/FABIO");
            firma.setdRecepc("2019-02-28 09:00");
            firma.setcDescri("CERTIFICADO DE ESTUDIOS");
            laFirmas.add(firma);
            pendientesAdapter.omSetListFirmas(laFirmas);
        }
        if (code == GET_FIRMAR) {
            fingerprintDialog.dismiss();
        }
    }

    @Override
    public void onFirmaClickListener(Firma firma) {
        FirmasActivity.this.iFingerprint = FirmasActivity.this;
        this.paData.put("CCODTRE", firma.getcCodTre());
        if (setupFingerprint()) {
            fingerprintDialog = new AlertDialog.Builder(FirmasActivity.this)
                    .setView(R.layout.dialog_firma)
                    .create();
            fingerprintDialog.show();
        }
        else {
            new AlertDialog.Builder(this)
                    .setTitle("Firmar")
                    .setMessage(String.format("%s DE %s", firma.getcDescri(), firma.getcNombre()))
                    .setPositiveButton("Firmar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            onAuthSuccess();
                        }
                    })
                    .create()
                    .show();
        }
    }

    @Override
    public void onAuthSuccess() {
        try {
            final JSONObject loJson = new JSONObject();
            JSONObject loData = new JSONObject();
            loJson.put("Id", "FirmarDocumento");
            loData.put("CCODTRE", this.paData.get("CCODTRE"));
            //loData.put("CCODUSU", omGetValSession("CCODUSU"));
            loData.put("CCODUSU", Sessions.getInstance().read("LOGIN").getString("CCODDOC"));
            loJson.put("paData", loData);
            fingerprintDialog.findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
            Log.e(TAG, loJson.toString());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    RestClient loClient = new RestClient();
                    loClient.execute("http://apps.ucsm.edu.pe/UCSMMTA/wsApp.php", loJson.toString(), GET_FIRMAR, FirmasActivity.this);
                }
            }, 1500);
        } catch (JSONException e) {
            e.printStackTrace();
            Snackbar.make(main_view, "ERROR EN CONSEGUIR DATOS PARA FIRMAR", Snackbar.LENGTH_SHORT).show();
        }
        //Snackbar.make(main_view, "SE FIRMO EL DOCUMENTO", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onAuthFailed(FingerprintHandler.FINGERPRINT_ERROR fingerprint_error, String message) {
        //Snackbar.make(main_view, message, Snackbar.LENGTH_SHORT).show();
        ((TextView)fingerprintDialog.findViewById(R.id.txtError)).setText(message);
        //Borrar mensaje alerta
        Handler loHandler = new Handler();
        loHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ((TextView)fingerprintDialog.findViewById(R.id.txtError)).setText("");
            }
        }, 1500);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSalir:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
        }
    }
}
