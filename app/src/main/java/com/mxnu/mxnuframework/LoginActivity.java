package com.mxnu.mxnuframework;

import android.app.Application;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.mxnu.mxnuframework.mf_files.Adapters.ListAdapter;
import com.mxnu.mxnuframework.mf_files.Adapters.TableAdapter;
import com.mxnu.mxnuframework.mf_files.CustomAdapter;
import com.mxnu.mxnuframework.mf_files.MxnuFramework;
import com.mxnu.mxnuframework.mf_files.OnCallbackClick;
import com.mxnu.mxnuframework.mf_files.Util.Sessions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manu2 on 04/01/2018.
 */

public class LoginActivity extends MxnuFramework implements OnCallbackClick {
    private static final String TAG = "LOGINACTIVITY";
    MxnuApplication app = (MxnuApplication) getApplication();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JSONObject loginJSON = Sessions.getInstance().read("LOGIN");
        if(loginJSON != null){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        loadView(R.raw.login_view);
    }

    @Override
    public void onCallbackClick(int id, Object message) {
        switch (id){
            case 1:

                break;
        }
    }
}
